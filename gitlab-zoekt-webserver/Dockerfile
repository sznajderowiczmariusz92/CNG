ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG FROM_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go"
ARG GO_TAG="master"
ARG TAG="master"
ARG GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${TAG}"

FROM --platform=${TARGETPLATFORM} ${FROM_IMAGE}:${GO_TAG} as builder

ARG BUILD_DIR=/tmp/build
ARG DOCKER_BUILDTAGS="include_oss include_gcs continuous_profiler_stackdriver"
ARG ZOEKT_VERSION=fcb279ae404c0aa102121b28257143ad16e77482
ARG ZOEKT_ARCHIVE_DOWNLOAD_URL="https://github.com/sourcegraph/zoekt/archive/$ZOEKT_VERSION.zip"
ARG GOPATH=/go
ARG REGISTRY_SOURCE_PATH=${GOPATH}/src/github.com/docker/distribution
ARG GITLAB_BASE_IMAGE

RUN buildDeps=' \
  git \
  unzip \
  wget' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && mkdir -p ${BUILD_DIR} \
  && cd ${BUILD_DIR} \
  && wget --output-document zoekt.zip "$ZOEKT_ARCHIVE_DOWNLOAD_URL" \
  && unzip zoekt.zip \
  && cd "zoekt-$ZOEKT_VERSION" \
  && CGO_ENABLED=0 go build -o /usr/local/bin/zoekt-webserver ./cmd/zoekt-webserver/ \
  && rm -rf ${GOPATH} \
  && apt-get purge -y --auto-remove $buildDeps \
  && rm -rf /var/lib/apt/lists/* \
  && cd / \
  && rm -rf ${BUILD_DIR}

## FINAL IMAGE ##

FROM --platform=${TARGETPLATFORM} ${GITLAB_BASE_IMAGE}

ARG GITLAB_USER=git
ENV PORT=6070

# create gitlab user
RUN apt-get update \
  && apt-get install -y --no-install-recommends universal-ctags \
  && rm -rf /var/lib/apt/lists/* \
  && adduser --disabled-password --gecos 'GitLab' ${GITLAB_USER}

# create data directories
RUN mkdir -p /data/index \
  && chown -R $GITLAB_USER:$GITLAB_USER /data

VOLUME ["/data/index"]

COPY --from=builder /usr/local/bin/zoekt-* /bin/
COPY scripts/ /scripts/

USER $GITLAB_USER:$GITLAB_USER

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
