## FINAL IMAGE ##

ARG GITLAB_BASE_IMAGE=

FROM ${GITLAB_BASE_IMAGE}

ARG GITLAB_PAGES_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG CONFIG_DIRECTORY=/etc/gitlab-pages
ARG DATA_DIRECTORY=/srv/gitlab-pages
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/gitlab-pages" \
      name="GitLab Pages" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_PAGES_VERSION} \
      release=${GITLAB_PAGES_VERSION} \
      summary="Serve static websites from GitLab repositories." \
      description="Serve static websites from GitLab repositories."

ADD gitlab-pages.tar.gz /
ADD gitlab-gomplate.tar.gz /

COPY scripts/ /scripts

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0  procps libicu tzdata shadow-utils \
    && microdnf clean all \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p ${CONFIG_DIRECTORY} ${DATA_DIRECTORY} /var/log/gitlab \
    && chown -R ${UID}:0 ${CONFIG_DIRECTORY} ${DATA_DIRECTORY} /var/log/gitlab /scripts \
    && chmod -R g=u ${CONFIG_DIRECTORY} ${DATA_DIRECTORY} /var/log/gitlab /scripts \
    # remove shadow-utils within the same layer
    && microdnf remove shadow-utils \
    && microdnf clean all

USER ${UID}

ENV CONFIG_TEMPLATE_DIRECTORY=/etc/gitlab-pages

CMD ["/scripts/start-pages"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 \
CMD /scripts/healthcheck
